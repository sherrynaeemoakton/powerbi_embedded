﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GRDC.PowerBIEmbedded.Startup))]
namespace GRDC.PowerBIEmbedded
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
        }
    }
}
